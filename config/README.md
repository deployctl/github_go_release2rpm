# configuration

This directory hold the configuration for the package.

Deployment location on target: `/etc/<package>/`

## sysconfig.package

### example

sysconfig.package
```
# Define the start-up options for the service
# Do not delete, needed by auto generated /etc/systemd/system/$package.service
#
OPTIONS="-config.path /etc/my_package/myconfig.whatever"
```

and this would need a `myconfig.whatever` to be created in this directory.

## extra_post.sh

Defines extra post install options:

### example

 My prometheus install, with predefined exporters on the same machine, would all refer to `localhost`, here in the `extra_post.sh`, we rename `localhost` in the prometheus config file to `mon.$hostname` and add this hostname to the /etc/hosts for `127.0.0.1`

This would be executed in the rpm post_install, before starting the service.

e.g.: extrapost.sh

```bash
#
# extra package specific actions
#
# replace localhost instance by mon.hostname
#
sed -i -e "s|localhost|mon.$(hostname)|" "/etc/${package}/${package}.yml"
#
# add mon.hostname to the /etc/host so it can be resolved
#
sed -i "/127.0.0.1/s/$/ mon.$(hostname)/" /etc/hosts
```

## optional config files for the service

any specific config files for the service.
