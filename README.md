# Purpose

Quickly turn a go package in a service, packed in an RPM, immediately available for installation and use.

this template will when configured, start a gitlab-ci pipeline

* download the package binary from github release page.
* extract the binary
* modify the init.d and systemd service files.

* build the rpm package

* deploy with deployctl to a deployctl Repository


## Supported systems

* init.d  => CentOS6
* systemd => CentOS7

For init.d script, the binary is started with the centos6 daemon function, and started in the background, redirects the `stdout` and `stderr` to `/var/log/<package>.log`

*Remark:* define the `$OPTIONS="..."` such as the binary does no fork and runs in the background.

## Usage:

Clone this project

1. set the template configuration in `gitlab-ci.yml`
2.  set `$OPTIONS` in the `sysconfig.package`
3. Remove this `Purpose` section in the readme and update template section below

Enable the deployctl runner for project.

Wait for job pipeline to finish

Setup repository at client:

```
curl -sS https://<myrepourl.mydomain.tld> | sudo bash
```

Install the new package:

```
sudo yum install <mynewpackage>
```
done.

## Template configurations:

### .gitlab-ci-yml

```yaml
# Template config Variables
variables:
  # change if gitlab project name is different then the github url
  package: $CI_PROJECT_NAME
  version: <upstream version>
  url: <github project url>
  description: "<description of the project>"
  license: "<project License>"
# Upstream arch
  source_arch: amd64
  # arch of RPM to be created here
  rpm_arch: x86_64
  #
  # Repository to deploy to
  DEPLOY_REPO_URL: "<deployctl repo url>"
  #
  #  END Template variables

```

### config directory

This directory hold the configuration for the package.

Deployment location on target: `/etc/<package>/`

##### sysconfig.package

contains the starup options variable of the binary

_example_

sysconfig.package
```
# Define the start-up options for the service
# Do not delete, needed
# /etc/systemd/system/package.service
# OR
# /etc/init.d/package
#
OPTIONS="-config.path /etc/my_package/myconfig.whatever"
```

and this would need a `myconfig.whatever` to be created in this directory.

#### extra_post.sh

Defines extra post install shell commands, this file is called from the rpm post install.

_example:_

 My prometheus install, with predefined exporters on the same machine, would all refer to `localhost:xxxx`, here in the `extra_post.sh`, we rename `localhost` in the prometheus config file to `mon.$hostname` and add this hostname to the /etc/hosts for `127.0.0.1`

This would be executed in the rpm post_install, before starting the service.

extrapost.sh

```bash
#
# extra package specific actions
#
# replace localhost instance by mon.hostname
#
sed -i -e "s|localhost|mon.$(hostname)|" "/etc/${package}/${package}.yml"
#
# add mon.hostname to the /etc/host so it can be resolved
#
sed -i "/127.0.0.1/s/$/ mon.$(hostname)/" /etc/hosts
```

#### optional config files for the service

any specific config files for the binary to use, see `sysconfig.package`.



# Template README

##  `<packagename>` RPM

### Purpose

RPM packaging for CentOS and deployment to https://`<repo_domain_name>`


* Sample config-file at `/etc/<packagename>/....`
* Data dir at `/var/lib/<packagename/>`
* service run as user `<packagename>`

Control service `<packagename>` with:

```
sudo systemctl [stop|start|restart|reload]
```

### INSTALL

```
curl https://<repo_domain_name>/repo.rpm.sh | sudo bash
yum install <packagename>
```

### Upstream info:

* source and binary: `[ <packagename> ]( <github_url> )`

* VERSION: 0.0.0

* description:

> `<paste upstream project description>`

* Upstream License:

>```
<paste upstream LICENSE content>
```
