#!/bin/sh
set +e

package=mypackage
is_systemd=0

daemon_stop () {
    [ "$is_systemd" = "1" ] && /bin/systemctl stop ${package} || /sbin/service ${package} stop
}

daemon_status () {
    [ "$is_systemd" = "1" ] && /bin/systemctl status ${package} > /dev/null 2>&1 || /sbin/service ${package} status > /dev/null 2>&1
}

set +e
/bin/systemctl --version > /dev/null 2>&1
[ "$?" = 0 ] && is_systemd=1

if [ "$1" = "2" ]; then
  daemon_status
  if [ "$?" = "0" ];
  then
    daemon_stop
    touch /tmp/ ${package}.stopped
  fi
fi
set -e
