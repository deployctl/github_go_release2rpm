#!/bin/sh
package=mypackage

is_systemd=0

daemon_disable () {
  [ "$is_systemd" = "1" ] && /bin/systemctl disable ${package} || /sbin/chkconfig ${package} off
}

daemon_stop () {
    [ "$is_systemd" = "1" ] && /bin/systemctl stop ${package} || /sbin/service ${package} stop
}

daemon_status () {
    [ "$is_systemd" = "1" ] && /bin/systemctl status ${package} > /dev/null 2>&1 || /sbin/service ${package} status > /dev/null 2>&1
}

set +e
/bin/systemctl --version > /dev/null 2>&1
[ "$?" = 0 ] && is_systemd=1


if [ "x$1" = "x0" ]; then
    set -e
    daemon_stop
    daemon_disable
    rm -rf /var/lib/${package}
    rm /etc/init.d/${package}
fi
