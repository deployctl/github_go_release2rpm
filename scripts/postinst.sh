#!/bin/sh
package=mypackage
is_systemd=0

daemon_reload () {
[ "$is_systemd" = "1" ] && /bin/systemctl daemon-reload
}

daemon_enable () {
  [ "$is_systemd" = "1" ] && /bin/systemctl enable ${package} || /sbin/chkconfig ${package} on
}

daemon_start () {
  [ "$is_systemd" = "1" ] && /bin/systemctl start ${package} || /sbin/service ${package} start
}

daemon_stop () {
    [ "$is_systemd" = "1" ] && /bin/systemctl stop ${package} || /sbin/service ${package} stop
}

daemon_status () {
    [ "$is_systemd" = "1" ] && /bin/systemctl status ${package} > /dev/null 2>&1 || /sbin/service ${package} status > /dev/null 2>&1
}
service_install () {
    if [ "$is_systemd" = "1" ]; then
      cp -f /etc/${package}/service/systemd.service /etc/systemd/system/${package}.service 
    else  
      cp -f /etc/${package}/service/init.d.service /etc/init.d/${package}
      chmod 755 /etc/init.d/${package}
    fi
}

set +e
/bin/systemctl --version > /dev/null 2>&1
[ "$?" = 0 ] && is_systemd=1

if [ "$1" = "1" ];
then
    service_install
    set +e
    /usr/bin/getent passwd ${package} > /dev/null 2>&1
    if  [ ! "$?" == 0 ]; then
      useradd -M -l ${package}
    fi
    source "/etc/${package}/extra_post.sh"
    mkdir -p /var/lib/${package}
    chown -R ${package} /var/lib/${package}
    chmod -R 750 /var/lib/${package}
fi
set +e

# If it is an upgrade, and
if [ "$1" = "2" ];
then
  service_install
  daemon-reload
  if [ -f /tmp/${package}.stopped ];
  then
      # we stopped a running server, start it.
	  echo "${package} was stopped, starting"
	  rm /tmp/${package}.stopped
	  daemon_start
	  daemon_status
	  if [ "$?" = "0" ];
	  then
echo -e "\033[0;32m"
echo "********************************************"
echo "  Success, ${package} Updated and restarted!"
echo "********************************************"
echo -e "\033[0;m"
	  else
echo -e "\033[0;31m"
echo "********************************************"
echo "Failed starting ${package} after update"
echo "********************************************"
echo -e "\033[0;m"
	  fi
  fi
fi

if [ "$1" = 1 ];
then
  daemon_enable
  daemon_start
  sleep 3
  set +e
  daemon_status
  if [ "$?" = "0" ];
  then
    echo -e "\033[0;32m"
    echo "*************************************************"
    echo "Success, ${package} installed and up and running!"
    echo "*************************************************"
    echo -e "\033[0;m"
  else
    echo -e "\033[0;31m"
    echo "********************************************"
    echo "Failed starting ${package}"
    echo "********************************************"
    echo -e "\033[0;m"
    echo "check log"
  fi
  set -e
fi
